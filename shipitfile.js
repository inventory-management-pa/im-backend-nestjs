module.exports = (shipit) => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('shipit-deploy')(shipit);
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('shipit-shared')(shipit);

  const appName = 'exchange-rate';

  shipit.initConfig({
    default: {
      deployTo: '/home/ubuntu/apps/',
      repositoryUrl:
        'https://priyanshu-deploy-token:tDzNVuY8UY5eu4_kpGRD@gitlab.com/exchange-rate1/exchange-rate.git',
      keepReleases: 3,
      shared: {
        overwrite: true,
        dirs: ['node_modules'],
      },
    },
    production: {
      servers: 'ubuntu@3.109.1.9',
      branch: 'master',
      key: '~/Documents/Work/"My Projectso"/ExchangeRate/deployment/ubuntu.pem', // path to ssh key
    },
    // development: {
    //   deployTo: '/home/ubuntu/apps/v3/development/api',
    //   servers: 'ubuntu@192.168.44.125',
    //   branch: 'development',
    // },
  });

  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const path = require('path');
  const ecosystemFilePath = path.join(
    shipit.config.deployTo,
    'ecosystem.config.js',
  );

  // sequence of deployment
  shipit.on('updated', async () => {
    shipit.start(
      'replace-environment',
      'npm-install',
      'npm-build',
      'copy-config',
    );
  });

  shipit.blTask('replace-environment', async () => {
    // await shipit.log('releasePath:', shipit.releasePath);
    await shipit.remote(
      `cd ${shipit.releasePath} && mv src/app-config/environments/${shipit.environment}.env src/app-config/environments/.env`,
    );
    await shipit.log(`copied to .env successfully`);
  });

  shipit.blTask('npm-install', async () => {
    await shipit.remote(`npm --version && node --version`);
    await shipit.remote(`cd ${shipit.releasePath} && npm install`);
  });

  shipit.blTask('npm-build', async () => {
    await shipit.remote(
      `cd ${shipit.releasePath} && NODE_ENV=${shipit.environment} npm run build`,
    );
  });

  shipit.blTask('copy-config', async () => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const fs = require('fs');
    const ecosystem = `
    module.exports = {
      apps: [
        {
          name: '${appName}-${shipit.environment}',
          script: '${shipit.releasePath}/dist/main.js',
          autorestart: true,
          restart_delay: 1000,
          env: {
            NODE_ENV: 'staging'
          },
          env_production: {
            NODE_ENV: 'production'
          }
        }
      ]
    };`;

    fs.writeFile('ecosystem.config.js', ecosystem, (err) => {
      if (err) {
        throw err;
      }
      console.log('ecosystem file successfully created.');
    });

    await shipit.copyToRemote('ecosystem.config.js', ecosystemFilePath);
  });

  shipit.on('published', async () => {
    shipit.start('pm2-server');
  });

  shipit.blTask('pm2-server', async () => {
    await shipit.remote(`pm2 delete -s ${appName}-${shipit.environment} || :`);
    await shipit.log(`starting pm2 for ${shipit.environment} environment`);
    await shipit.remote(
      `cd ${shipit.releasePath} && pm2 start ${ecosystemFilePath} --env ${shipit.environment}`,
    );
  });
};
