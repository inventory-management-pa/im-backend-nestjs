import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AppConfigModule } from '../app-config/app-config.module';
import { AppConfigService } from '../app-config/app-config.service';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [AppConfigModule],
      useFactory: (
        appConfigService: AppConfigService,
      ): TypeOrmModuleOptions => ({
        type: 'postgres',
        host: appConfigService.databaseHost,
        port: appConfigService.databasePort,
        username: appConfigService.databaseUsername,
        password: appConfigService.databasePassword,
        database: appConfigService.databaseSchema,
        entities: ['dist/**/*.entity{.ts,.js}'],
        synchronize: true,
        // https://orkhan.gitbook.io/typeorm/docs/logging
        logging: appConfigService.databaseLogging ? 'all' : false,
        // log slow queries
        maxQueryExecutionTime: appConfigService.databaseMaxQueryExecutionTime,
      }),
      inject: [AppConfigService],
    }),
  ],
  exports: [TypeOrmModule],
})
export class DatabaseConfigModule {}
