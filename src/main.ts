import { ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppConfigService } from './app-config/app-config.service';
import { AppModule } from './app.module';
import {
  GeneralExceptionFilter,
  ValidationExceptionFilter,
} from './shared/exceptions/filters';
import { ApiResponseInterceptor } from './shared/interceptor/api-response.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  app.useGlobalInterceptors(new ApiResponseInterceptor(app.get(Reflector)));
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalFilters(
    new GeneralExceptionFilter(),
    new ValidationExceptionFilter(),
  );

  const appConfigService = app.get(AppConfigService);

  const config = new DocumentBuilder()
    .setTitle('Inventory Management')
    .setDescription('Inventory Management API')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api', app, document);

  await app.listen(appConfigService.port);
}
bootstrap();
