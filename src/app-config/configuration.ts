export default () => ({
  port: parseInt(process.env.PORT) || 3000,
  db: {
    host: process.env.IM_DATABASE_HOST,
    port: process.env.IM_DATABASE_PORT,
    username: process.env.IM_DATABASE_USER,
    password: process.env.IM_DATABASE_PASS,
    schema: process.env.IM_DATABASE_SCHEMA,
    charset: process.env.IM_DATABASE_CHARSET,
    logging: process.env.IM_DB_LOGGING,
    et: process.env.IM_MAX_QUERY_EXECUTION_TIME,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expire: parseInt(process.env.JWT_EXPIRE),
  },
});
