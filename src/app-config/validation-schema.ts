import * as Joi from 'joi';

const schema = Joi.object({
  PORT: Joi.number().required(),
  IM_DATABASE_HOST: Joi.string().required(),
  IM_DATABASE_PORT: Joi.number().required(),
  IM_DATABASE_USER: Joi.string().required(),
  IM_DATABASE_PASS: Joi.string().required(),
  IM_DATABASE_SCHEMA: Joi.string().required(),
  IM_DATABASE_CHARSET: Joi.string().required(),
  JWT_SECRET: Joi.string().required(),
  JWT_EXPIRE: Joi.number().required(),
});

export default schema;
