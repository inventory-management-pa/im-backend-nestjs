import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfigService {
  constructor(private configService: ConfigService) {}

  get port(): number {
    return this.configService.get<number>('port');
  }

  get databaseHost(): any {
    return this.configService.get<number>('db.host');
  }

  get databasePort(): any {
    return this.configService.get<number>('db.port');
  }

  get databaseUsername(): any {
    return this.configService.get<number>('db.username');
  }

  get databasePassword(): string {
    return this.configService.get<string>('db.password');
  }

  get databaseSchema(): string {
    return this.configService.get<string>('db.schema');
  }

  get databaseCharset(): string {
    return this.configService.get<string>('db.charset');
  }

  get databaseLogging(): number {
    return this.configService.get<number>('db.logging');
  }

  get databaseMaxQueryExecutionTime(): any {
    return this.configService.get<number>('db.et');
  }

  get jwtSecret(): string {
    return this.configService.get<string>('jwt.secret');
  }

  get jwtExpire(): number {
    return this.configService.get<number>('jwt.expire');
  }
}
