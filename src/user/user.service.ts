import { Injectable } from '@nestjs/common';
import { SignUpDto } from 'src/auth/dtos/signup.dto';
import { GeneralException } from 'src/shared/exceptions/handlers';
import { ApiErrors } from 'src/shared/response/messages';
import { User } from './entities/user.entity';
import { UserRepository } from './repositories/user.repository';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  getUserById(userId: number) {
    return this.userRepository.findOne({ id: userId });
  }

  getUserByEmail(email: string) {
    return this.userRepository.findOne({ email });
  }

  async createUser(signupDto: SignUpDto): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { email: signupDto.email },
    });

    if (user) throw new GeneralException(ApiErrors.EmailAlreadyExists);

    return this.userRepository.createUser(signupDto);
  }
}
