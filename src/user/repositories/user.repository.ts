import { SignUpDto } from 'src/auth/dtos/signup.dto';
import bcrypt from 'src/shared/utils/bcrypt';
import { EntityRepository, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  public async createUser(userSignupDto: SignUpDto): Promise<User> {
    const user = new User();
    user.firstName = userSignupDto.firstName;
    user.lastName = userSignupDto.firstName;
    user.email = !!userSignupDto.email ? userSignupDto.email : '';
    user.phone = !!userSignupDto.phone ? userSignupDto.phone : '';
    user.saltKey = await bcrypt.generateSalt();
    user.password = !!userSignupDto.password
      ? await bcrypt.generatePasswordHash(userSignupDto.password, user.saltKey)
      : '';
    return this.save(user);
  }
}
