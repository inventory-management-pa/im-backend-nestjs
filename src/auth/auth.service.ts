import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { GeneralException } from 'src/shared/exceptions/handlers';
import { ApiErrors } from 'src/shared/response/messages';
import bcrypt from 'src/shared/utils/bcrypt';
import { UserService } from 'src/user/user.service';
import { LoginDto } from './dtos/login.dto';
import { SignUpDto } from './dtos/signup.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {}

  async signup(signUpDto: SignUpDto) {
    const { email, firstName, lastName } = await this.userService.createUser(
      signUpDto,
    );

    return {
      email,
      firstName,
      lastName,
    };
  }

  async login(loginDto: LoginDto) {
    const { email, password } = loginDto;
    const user = await this.userService.getUserByEmail(email);
    // Validate user credentials.
    const isValidated = await bcrypt.compare(password, user.password);

    if (!isValidated) {
      throw new GeneralException(ApiErrors.WrongCredentials);
    }
    // Generate access token.
    const tokenPayload = { sub: user.id };
    return { token: this.jwtService.sign(tokenPayload) };
  }
}
