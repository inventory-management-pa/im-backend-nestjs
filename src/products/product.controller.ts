import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/shared/decorators/get-user.decorator';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { User } from 'src/user/entities/user.entity';
import { AddProductDto } from './dtos/add-products.dto';
import { GetProductsDto } from './dtos/get-products.dto';
import { ProductService } from './product.service';

@ApiTags('products')
@UseGuards(JwtAuthGuard)
@Controller('products')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Get('/')
  async getProducts(
    @GetUser() user: User,
    @Query() getProductsDto: GetProductsDto,
  ) {
    return this.productService.getProducts(user, getProductsDto);
  }

  @Post('/product')
  async addProduct(
    @GetUser() user: User,
    @Body() addProductDto: AddProductDto,
  ) {
    return this.productService.addProduct(user, addProductDto);
  }
}
