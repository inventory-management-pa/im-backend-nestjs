import { User } from 'src/user/entities/user.entity';
import { Repository, EntityRepository } from 'typeorm';
import { GetProductsDto } from '../dtos/get-products.dto';
import { Product } from '../entities/product.entity';

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  getProducts(user: User, getProductsDto: GetProductsDto) {
    // Filters
    return this.find({ user });
  }
}
