import { Store } from 'src/store/entities/store.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Store)
export class StoreRepository extends Repository<Store> {
  getStoreById(storeId: number): Promise<Store> {
    return this.findOne({ id: storeId });
  }
}
