import { EntityRepository, Repository } from 'typeorm';
import { Category } from '../entities/category.entity';

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  getCategoryById(categoryId: number): Promise<Category> {
    return this.findOne({ id: categoryId });
  }
}
