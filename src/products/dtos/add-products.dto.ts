import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsInt, IsOptional, IsString, Min } from 'class-validator';

export class AddProductsDto {
  @ApiProperty()
  @IsArray()
  products: AddProductDto[];
}

export class AddProductDto {
  @ApiProperty()
  @IsInt()
  @Min(0)
  categoryId: number;

  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsInt()
  @Min(0)
  price: number;

  @ApiProperty()
  @IsOptional()
  @IsInt()
  @Min(0)
  storeId?: number;

  @ApiProperty()
  @IsOptional()
  @IsInt()
  @Min(0)
  brandId?: number;
}
