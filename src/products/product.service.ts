import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Store } from 'src/store/entities/store.entity';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { AddProductDto, AddProductsDto } from './dtos/add-products.dto';
import { GetProductsDto } from './dtos/get-products.dto';
import { Brand } from './entities/brand.entity';
import { Category } from './entities/category.entity';
import { Product } from './entities/product.entity';
import { BrandRepository } from './repositories/brand.repository';
import { CategoryRepository } from './repositories/category.repository';
import { ProductRepository } from './repositories/product.repository';
import { StoreRepository } from './repositories/store.repository';

@Injectable()
export class ProductService {
  constructor(
    private readonly productRepository: ProductRepository,
    private readonly brandRepository: BrandRepository,
    private readonly categoryRepository: CategoryRepository,
    private readonly storeRepository: StoreRepository,
  ) {}

  getProducts(user: User, getProductsDto: GetProductsDto) {
    return this.productRepository.getProducts(user, getProductsDto);
  }

  async addProduct(user: User, addProductDto: AddProductDto): Promise<Product> {
    const { name, brandId, price, categoryId, storeId } = addProductDto;
    const product = new Product();
    product.name = name;
    product.price = price;
    product.category = await this.getCategoryById(categoryId);
    product.user = user;
    if (brandId) product.brand = await this.getBrandById(brandId);
    if (storeId) product.store = await this.getStoreById(storeId);

    return this.saveProduct(product);
  }

  async saveProduct(product: Product): Promise<Product> {
    return await this.saveProducts([product])[0];
  }

  saveProducts(products: Product[]) {
    return this.productRepository.save(products);
  }

  // Brand Functions
  getBrandById(brandId: number): Promise<Brand> {
    return this.brandRepository.getBrandById(brandId);
  }

  // Category Functions.
  getCategoryById(categoryId: number): Promise<Category> {
    return this.categoryRepository.getCategoryById(categoryId);
  }

  // Store Functions.
  getStoreById(storeId: number): Promise<Store> {
    return this.storeRepository.getStoreById(storeId);
  }
}
