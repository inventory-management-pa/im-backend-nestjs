import _bcrypt = require('bcryptjs');
import { generateSHA256Hash } from './sha256';

const generateSalt = async (rounds = 10): Promise<string> => {
  return _bcrypt.genSalt(rounds);
};

const generatePasswordHash = async (
  password: string,
  salt: string,
): Promise<string> => {
  return _bcrypt.hash(generateSHA256Hash(password), salt);
};

const compare = async (data: string, hash: string): Promise<boolean> => {
  return _bcrypt.compare(generateSHA256Hash(data), hash);
};

const bcrypt = { generateSalt, generatePasswordHash, compare };

export default bcrypt;
