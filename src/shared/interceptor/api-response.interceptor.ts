import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { isString } from 'class-validator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponseSuccess } from '../types/api-response.interface';

@Injectable()
export class ApiResponseInterceptor implements NestInterceptor {
  constructor(private reflector: Reflector) {}
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((value) => {
        const apiResponse: ApiResponseSuccess = {};

        if (isString(value)) {
          apiResponse.message = value;
        } else {
          apiResponse.payload = value;
        }

        return apiResponse;
      }),
    );
  }
}
