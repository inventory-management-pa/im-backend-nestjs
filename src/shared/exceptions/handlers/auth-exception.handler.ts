import { HttpException } from '@nestjs/common';
import { ApiError } from '../../response/messages/api-errors';

export class AuthException extends HttpException {
  constructor(error: ApiError) {
    super(error, error.statusCode);
  }
}
