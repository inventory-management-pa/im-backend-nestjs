import { HttpStatus } from '@nestjs/common';

export interface ApiError {
  message: string;
  code: number;
  statusCode: HttpStatus;
}

const errorCreator = (
  message: string,
  code: number,
  statusCode: HttpStatus = HttpStatus.BAD_REQUEST,
): ApiError => ({ message, code, statusCode });

export const ApiErrors = {
  SystemErrorMessage: errorCreator(
    'Internal server error, please try again later',
    100,
  ),
  GeneralError: errorCreator(
    'Something went wrong please try again later',
    100,
  ),
  InvalidAccessToken: errorCreator('Invalid access token', 100),
  EmailAlreadyExists: errorCreator('Email already exists', 100),
  WrongCredentials: errorCreator('Wrong credentials', 100),
};
