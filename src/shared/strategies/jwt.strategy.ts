import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from 'src/user/user.service';
import { AppConfigService } from '../../app-config/app-config.service';
import { User } from '../../user/entities/user.entity';
import { AuthException } from '../exceptions/handlers';
import { ApiErrors } from '../response/messages';
import { JwtPayload } from '../types/jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private userService: UserService,
    private appConfigService: AppConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: appConfigService.jwtSecret,
      passReqToCallback: true,
    });
  }

  async validate(request: Request, { sub: userId }: JwtPayload): Promise<User> {
    const user: User = await this.userService.getUserById(userId);

    if (!user) {
      throw new AuthException(ApiErrors.InvalidAccessToken);
    }

    return user;
  }
}
